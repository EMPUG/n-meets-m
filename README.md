# N-meets-M

The purpose of n-meets-m is to coordinate the activity of _m_ providers and
_n_ consumers.

Consider a bake sale for a charity. Some people (the providers) volunteer
to bake items, and others (the consumers) agree to purchase the items. This
is easy to coordinate if the bake sale occurs in person; people show up with
food and people buy the food. If the bake sale is virtual (with the transfer of
the items occurring as a separate step), coordination is more challenging.

_n-meets-m_ will keep track of:

- a campaign specifying the items to be provided and consumed
- users who agree to provide items
- users who agree to consume items
- which items are still availabe
